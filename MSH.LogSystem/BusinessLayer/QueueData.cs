﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class QueueData
    {
        public string QueueName { get; set; }

        public LogRequest LogRequest { get; set; }
    }
}
