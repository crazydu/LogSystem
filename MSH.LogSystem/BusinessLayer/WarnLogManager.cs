﻿using BusinessLayer.Interface;
using DTO;
using ElasticSearchAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class WarnLogManager : IWarnLogManager
    {
        private WarnLogAccess _WarnLogAccess = new WarnLogAccess();

        public void DeleteLog(string id)
        {
            _WarnLogAccess.DeleteLog(id);
        }

        public void DeleteLog(List<string> ids)
        {
            _WarnLogAccess.DeleteLog(ids);
        }

        public List<LogInfo> QueryLogInfo(LogQuery logQuery)
        {
            var dao = new WarnLogAccess();
            return dao.QueryLogRequest(logQuery);
        }
    }
}
