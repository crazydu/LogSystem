﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSH.LogClient
{
    /// <summary>
    /// 本地存储LiteDB数据格式
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class LiteDbData<T>
    {
        public LiteDbData()
        {
        }

        public LiteDbData(T obj, int logLevel, string configName)
        {
            this.Obj = obj;
            this.LogLevel = logLevel;
            this.ConfigName = configName;
        }

        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 日志级别
        /// </summary>
        public int LogLevel { get; set; }
        /// <summary>
        /// 来源配置名称
        /// </summary>
        public string ConfigName { get; set; }

        public T Obj { get; set; }
    }
}
