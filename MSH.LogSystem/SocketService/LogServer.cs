﻿using Common;
using DTO;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocket.Core;

namespace SocketService.Core
{
    public class LogServer : AppServer<LogSession>
    {
        public LogServer()
            : base(new DefaultReceiveFilterFactory<LogReceiveFilter, StringRequestInfo>())
        {
        }

        protected override void OnStarted()
        {
            base.OnStarted();
        }

        protected override void OnStopped()
        {
            base.OnStopped();
        }

        protected override bool RegisterSession(string sessionID, LogSession appSession)
        {
            return base.RegisterSession(sessionID, appSession);
        }

        protected override void OnNewSessionConnected(LogSession session)
        {
            base.OnNewSessionConnected(session);
        }

        protected override void OnSessionClosed(LogSession session, CloseReason reason)
        {
            base.OnSessionClosed(session, reason);
        }

        protected override void OnSystemMessageReceived(string messageType, object messageData)
        {
            base.OnSystemMessageReceived(messageType, messageData);
        }

        protected override void ExecuteCommand(LogSession session, StringRequestInfo requestInfo)
        {
            base.ExecuteCommand(session, requestInfo);
        }

        protected override void UpdateServerStatus(StatusInfoCollection serverStatus)
        {
            base.UpdateServerStatus(serverStatus);
        }

        protected override void OnServerStatusCollected(StatusInfoCollection bootstrapStatus, StatusInfoCollection serverStatus)
        {
            var info = GetTcpServiceInfo(serverStatus);
            //发出通知
            WebSocketManage.SendToAll(info);
            base.OnServerStatusCollected(bootstrapStatus, serverStatus);
        }

        protected TcpServiceInfo GetTcpServiceInfo(StatusInfoCollection serverStatus)
        {
            return new TcpServiceInfo()
            {
                CreatTime = serverStatus.CollectedTime,
                AvialableSendingQueueItems = serverStatus.GetValue("AvialableSendingQueueItems", 0),
                IsRunning = serverStatus.GetValue("IsRunning", false),
                MaxConnectionNumber = serverStatus.GetValue("MaxConnectionNumber", 0),
                RequestHandlingSpeed = $"{serverStatus["RequestHandlingSpeed"]}",
                ServiceName = serverStatus.Name,
                StartedTime = serverStatus.GetValue("StartedTime", DateTime.Now),
                TotalConnections = serverStatus.GetValue("TotalConnections", 0),
                TotalHandledRequests = $"{serverStatus["TotalHandledRequests"]}",
                TotalSendingQueueItems = $"{serverStatus["TotalSendingQueueItems"]}",
            };
        }
    }
}
